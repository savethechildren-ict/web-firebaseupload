# web-firebaseupload
- Node helper for uploading Firebase data
- data to be uploaded in Database using array id's
- corresponding photos to be uploaded in Storage using the database's array id's

### Setup: Sanitize data
1. Check data in `data/shc-testing-centers.json`
2. Check images in `images/:id/*.jpg`

### Setup: Local Testing
1. node app.js
