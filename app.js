// Setup: modules
var fs = require('fs');
var path = require('path');
var admin = require('firebase-admin');

// Setup: firebase
var serviceAccount = require('./firebase-keys/safely-prod.json');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://safely-prod-25569.firebaseio.com'
});
var database = admin.database();

// Setup: google-cloud
var gcs = require('@google-cloud/storage')({
    projectId: 'safely-prod-25569',
    keyFilename: './firebase-keys/safely-prod.json'
});
var bucket = gcs.bucket('safely-prod-25569.appspot.com');

/*
// Read and import JSON
fs.readFile('./data/shcs.json', 'utf8', function (err, data) {
    if (err) throw err; 
    database.ref().set(JSON.parse(data));
});
*/

// Read images
var pathImage = path.join(__dirname, 'images');
var files = fs.readdirSync(pathImage);

files.forEach(file => {
    var pathImageCenter = path.join(pathImage, file);

    if (fs.lstatSync(pathImageCenter).isDirectory()) {
        console.log(file + ': is a directory');

        var uploads = fs.readdirSync(pathImageCenter);

        if (uploads.length === 0) {
            console.log('Directory doesn\'t contain any files; skip');
            return;
        }

        uploads.forEach(upload => {
            if (upload === '.DS_Store') {
                console.log('Skipping: .DS_Store');
                return;
            }
            console.log('to be uploaded: ' + upload);

            var uploadName = file + '/'  + upload;

            // Upload image
            var options = {
                destination: uploadName,
                resumable: true,
                validation: 'crc32c'
            };
            
            bucket.upload(
                './images/'+ file +'/' + upload, 
                options, 
                function(err, uploadedFile) {
                    if (err) {
                        console.error(err);
                        return;
                    } else {
                        if (uploadedFile === undefined) {
                            console.log('err is fine, but uploadedFile is undefined: ' + uploadName);
                        };
                        
                        uploadedFile.getSignedUrl({
                                action: 'read',
                                expires: '12-30-2050'
                            },
                            function(err, url) {
                                if (err) {
                                    console.error(err);
                                    return;
                                }

                                // push storage url to database
                                var pushId = database.ref().child('shcs/' + file + '/images').push().key;
                                database.ref('shcs/' + file + '/images/' + pushId).update({
                                    url: url
                                });
                            });
                    }
            });
        });
    } else if (fs.lstatSync(pathImageCenter).isFile()) {
        console.log(file + ': is a file');
    } else {
        console.log(file + ': unknown');
    }
});